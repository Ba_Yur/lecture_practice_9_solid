void main(List<String> arguments) {

  cla.area();

}


class Circle {
  final double radius;

  Circle(this.radius);

  double area() {
    double area = 3.14 * (this.radius * this.radius);
    return area;
  }
}

class Square {
  final double length;

  Square(this.length);

  double area() {
    double area = this.length * this.length;
    return area;
  }
}

class AreaCalculator {
  final List<dynamic> shapes;

  AreaCalculator(this.shapes);

  void output() {
    print('hola');
    print(shapes);
  }

  void sum() {
    shapes.forEach((element) {
      print(element.area());
    });
  }
}

List<dynamic> listOfShapes = [Circle(20), Square(20)];

AreaCalculator areaCalc = AreaCalculator(listOfShapes);

class NewClass extends AreaCalculator {
  NewClass(List shapes) : super(null);
  @override
  void sum() {
    super.sum();
  }
}

abstract class AreaImplementation implements Square {

  @override
  double get length => 30;

  @override
  double area() {
    double area = this.length * 2;
    return area;
  }
}

abstract class SomeAbstractClass implements AreaImplementation {

  @override
  double get length => 20;

  @override
  double area() {
    double area = this.length * 3;

    return area;
  }
}

class AnotherClass implements SomeAbstractClass {

  @override
  double get length => 10;
  @override
  double area() {
    double area = this.length * 4;
    print(area);
    return area;
  }
}

AnotherClass cla = AnotherClass();